#ifndef _GLYPH_H_
#define _GLYPH_H_

#include "color.h"
#include <istream>

using Mine::TypeColor;

class Glyph
{

public:

    long symbol;

    EColor fg;
    EColor bg;

    TypeColor foreground = TypeColor::GRAY;
    TypeColor background = TypeColor::BLACK;

    Glyph( );

    Glyph( long S, EColor F, EColor B, TypeColor nForeground, TypeColor nBackground );

    bool operator==( const Glyph &rhs );

    std::string save_data( );

    void load_data( std::istream &datastream );

};

#endif
