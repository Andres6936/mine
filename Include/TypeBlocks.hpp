#ifndef MINERL_TYPEBLOCKS_HPP
#define MINERL_TYPEBLOCKS_HPP

#include "Tile.hpp"

using Mine::TTile;

class TypeBlocks
{

public:

    TypeBlocks( ) = default;

    const TTile NONE = TTile( false, false, 0, 0, 0, "", "NULL" );
    const TTile EMPTY = TTile( false, false, 0, 0, 0, ".", "empty" );

    const TTile SKY = TTile( false, false, 0, 0, 0, "", "sky" );
    const TTile COAL = TTile( true, true, 5, 0, 5, "#", "coal" );
    const TTile IRON = TTile( true, true, 8, 0, 10, "#", "iron" );
    const TTile DIRT = TTile( true, false, 3, 0, 0, "   `~,.'^", "dirt" );
    const TTile SHOP = TTile( false, false, 0, 0, 0, "=", "shop" );
    const TTile GRASS = TTile( false, false, 0, 0, 0, "_", "grass" );
    const TTile COPPER = TTile( true, true, 10, 0, 14, "#", "copper" );
    const TTile LADDER = TTile( false, false, 0, 1, 0, "H", "ladder" );
    const TTile BOULDER = TTile( true, false, 0, 0, 0, "0", "boulder" );
    const TTile SUPPORT = TTile( false, true, 0, 0, 0, "|", "support" );
    const TTile ENTRANCE = TTile( false, false, 0, 0, 0, "M", "entrance" );
    const TTile LADDER_AND_SUPPORT = TTile( false, true, 0, 1, 0, "#", "support" );
};

#endif //MINERL_TYPEBLOCKS_HPP
