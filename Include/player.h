#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Entity.hpp"
#include "Direction.hpp"
#include "map.h"
#include "Inventory.hpp"

#include <vector>

class Player : public Entity
{

private:

    short stamina = 0;
    short maxStamina = 150;

    Coordinate2D posicion = Coordinate2D( 60, 12 );
    Mine::Inventory inventory = Mine::Inventory( );

    Cartographer *map = nullptr;

public:

    explicit Player( Cartographer &nMap );

    ~Player( ) override;

    void WalkAt( Mine::Direction nDirection ) override;

    void PutToolOfSupportInMap( Supplies tool ) override;

    void AttackEnemy( Monster &objetive );

    void AddMineralOreAtInventory( MaterialBlock mineral );

    void ReduceStamina( short amount );

    void PutLadder( );

    void PutSupport( );

    bool HasEnoughLadders( );

    bool HasEnoughSupport( );

    int GetDamage( );

    const int GetCoordinateX( ) override;

    const int GetCoordinateY( ) override;

    const Coordinate2D &GetCoordinate( ) override;

};


#endif
