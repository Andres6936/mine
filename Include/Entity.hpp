#ifndef MINERL_ENTITY_HPP
#define MINERL_ENTITY_HPP

#include <Geometry/Coordinate2D.hpp>
#include "Direction.hpp"
#include "Tool.hpp"

class Entity
{

public:

    Entity( );

    virtual ~Entity( );

    virtual void WalkAt( Mine::Direction nDirection ) = 0;

    virtual void PutToolOfSupportInMap( Supplies tool ) = 0;

    virtual const int GetCoordinateX( ) = 0;

    virtual const int GetCoordinateY( ) = 0;

    virtual const Coordinate2D & GetCoordinate( ) = 0;
};


#endif //MINERL_ENTITY_HPP
