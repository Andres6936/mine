#ifndef _TILEDATA_H_
#define _TILEDATA_H_

#include "color.h"
#include <string>

namespace Mine
{
    class TTile
    {

    private:

        TypeColor colorFore = TypeColor::GRAY;
        TypeColor colorBack = TypeColor::BLACK;

        bool blocks;
        bool support;

        int dig;
        int climbCost;
        int value;

        std::string symbols;
        std::string name;

    public:

        // Getters

        TypeColor getColorForeground( ) const;

        TypeColor getColorBackground( ) const;

        bool isBlocks( ) const;

        bool isSupport( ) const;

        int getDig( ) const;

        int getClimbCost( ) const;

        int getValue( ) const;

        const std::string &getSymbols( ) const;

        // Setters

        bool valuable( );

        TTile( );

        TTile( bool B, bool S, int D, int CC, int V, std::string Sym, std::string Name );

        TTile( TypeColor foreground, TypeColor background, bool B, bool S, int D, int CC, int V, std::string Sym,
               std::string Name );

    };
}

enum MaterialBlock
{
    T_null = 0,
    T_empty,
    T_sky,
    T_grass,
    T_shop,
    T_dirt,
    T_boulder,
    T_ladder,
    T_support,
    T_ladder_and_support,
    T_entrance,

    T_coal,
    T_iron,
    T_copper,

    T_max
};

#endif
