#ifndef MINERL_INVENTORY_HPP
#define MINERL_INVENTORY_HPP

#include <map>

#include "Tile.hpp"

#include "Tool.hpp"

namespace Mine
{
    class Inventory
    {

    public:

        Inventory( );

        void AddMineralOre( MaterialBlock mineral );

        void ReduceLaddersInAUnit( );

        void ReduceSupportsInAUnit( );

        bool HasEnoughLadders( );

        bool HasEnoughSupports( );

    private:

        std::map <MaterialBlock, int> minerals;
        std::map <Supplies, int> tools;

    };
}

#endif //MINERL_INVENTORY_HPP
