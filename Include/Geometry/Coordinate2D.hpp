#ifndef MINERL_COORDINATE2D_HPP
#define MINERL_COORDINATE2D_HPP

class Coordinate2D
{

public:

    int x;
    int y;

    Coordinate2D( );

    Coordinate2D( int X, int Y );

};

#endif //MINERL_COORDINATE2D_HPP
