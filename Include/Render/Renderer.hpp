#ifndef MINERL_RENDERER_HPP
#define MINERL_RENDERER_HPP

#include <Glyph.h>
#include <Entity.hpp>

class Renderer
{

public:

    Renderer( );

    virtual ~Renderer( );

    virtual void Refresh( ) = 0;

    virtual void Clear( ) = 0;

    virtual void DrawCharAt( int X, int Y, int Char ) = 0;

    virtual void DrawCharAt( int X, int Y, int Char, TypeColor foreground, TypeColor background ) = 0;

    virtual void DrawCharAt( int X, int Y, Glyph Char ) = 0;

    virtual void DrawStringAt( int X, int Y, std::string String ) = 0;

    virtual void DrawStringAt( int X, int Y, std::string String, TypeColor foreground, TypeColor background ) = 0;

    virtual void DrawInterface( ) = 0;

    virtual int GetKeyPressed( ) = 0;

    virtual void HandleInputUser( int nKeyPressed, Entity &nEntity ) = 0;

};


#endif //MINERL_RENDERER_HPP
