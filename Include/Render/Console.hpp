#ifndef MINERL_CONSOLE_HPP
#define MINERL_CONSOLE_HPP

#include <Glyph.h>
#include "BearLibTerminal.hpp"
#include "Renderer.hpp"

class Console : public Renderer
{

private:

    void SetBackgroundColor( TypeColor color );

    void SetForegroundColor( TypeColor color );

public:

    Console( );

    ~Console( ) override;

    void Refresh( ) override;

    void Clear( ) override;

    void DrawCharAt( int X, int Y, int Char ) override;

    void DrawCharAt( int X, int Y, int Char, TypeColor foreground, TypeColor background ) override;

    void DrawCharAt( int X, int Y, Glyph Char ) override;

    void DrawStringAt( int X, int Y, std::string String ) override;

    void DrawStringAt( int X, int Y, std::string String, TypeColor foreground, TypeColor background ) override;

    void DrawInterface( ) override;

    int GetKeyPressed( ) override;

    void HandleInputUser( int nKeyPressed, Entity &player ) override;

};


#endif //MINERL_CONSOLE_HPP
