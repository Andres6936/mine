#ifndef _MONSTER_H_
#define _MONSTER_H_

#include "Tile.hpp"
#include "Glyph.h"
#include "geometry.h"
#include <vector>
#include <cstdarg>  // For variadic constructor

struct Monster_type
{
    int hp;
    int dig;
    int dig_delay;
    int damage;
    int awareness;
    bool tools;
    bool climb;
    bool cling;
    bool fly;
    Glyph sym;
    std::vector <MaterialBlock> targets;

    Monster_type( )
    {
        hp = 1;
        dig = 1;
        dig_delay = 1;
        damage = 1;
    }

    Monster_type( int H, int D, int DD, int DAM, int A,
                  bool T, bool C, bool G, bool F,
                  char S, EColor FG, ... ) :
            hp( H ), dig( D ), dig_delay( DD ), damage( DAM ), awareness( A ),
            tools( T ), climb( C ), cling( G ), fly( F )
    {
        sym = Glyph( S, FG, c_black, TypeColor::BLUE, TypeColor::BLACK );
        va_list ap;
        va_start( ap, FG );
        MaterialBlock tmp;
        while (( tmp = ( MaterialBlock ) (va_arg( ap, int ))))
        {
            targets.push_back( tmp );
        }
        va_end( ap );
    }

};

struct Monster
{
    Monster( );

    Monster_type *type;
    int hp;
    int posx;
    int posy;

    void TakeDamage( int nDamage );

};

#endif
