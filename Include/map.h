#ifndef _MAP_H_
#define _MAP_H_

#include <Render/Renderer.hpp>

#include "Tile.hpp"
#include "window.h"
#include "geometry.h"
#include "monster.h"
#include "TypeBlocks.hpp"
#include <vector>
#include <Geometry/Coordinate2D.hpp>

using Mine::TTile;

namespace Mine
{
    class Block
    {

    private:

        // Fields

        bool gravity;
        bool isSeen;
        bool walkable;

        int durability;

    public:

        MaterialBlock material;

        Glyph representation;

        // Getters

        bool isWalkable( ) const;

        bool isSolid( ) const;

        bool isGravity( ) const;

        bool isSeen1( ) const;

        bool isMineralBlock( ) const;

        int getDurability( ) const;

        // Setters

        void setWalkable( bool isWalkable );

        void setGravity( bool isGravity );

        void setIsSeen( bool nIsSeen );

        void setMaterial( MaterialBlock nMaterial );

        void reduceDurability( int amount );


        // Constructs

        Block( );
    };
}

class Cartographer
{

private:

    std::vector <Mine::Block> tiles;

    std::vector <std::string> messages;

    Mine::Block null_tile;

public:

    short const WIDTH;
    short const HEIGHT;

public:

    Cartographer( short width, short heigth );

    ~Cartographer( );

    void IliminateAreaAroundOfPoint( Coordinate2D coordinate, short radius );

    void ProcessBlockWithGravity( );

    void ProcessMovementOfEntities( );

    void UpdateBlocks( Entity &entity );

    void UpdateEntities( Entity &entity );

    void SpawmEnemiesOfWayRandom( );

    void PutLadderAt( Coordinate2D nCoordinate2D );

    void PutSupportAt( Coordinate2D nCoordinate2D );

    int GetIndex( int X, int Y );

    int GetIndex( Coordinate2D coordinate );

    bool ExistEnemiesAt( Coordinate2D coordinate );

    bool IsSolidBlockAt( Coordinate2D coordinate );

    bool CanDigBlock( Coordinate2D coordinate );

    Monster &GetEnemyAt( Coordinate2D coordinate );

    MaterialBlock DigBlockAt( Coordinate2D coordinate );

    void reset_null_tile( );

    void add_vein( MaterialBlock type, int x, int y, int size );

    Mine::Block *get_tile( int x, int y );

    MaterialBlock get_tile_id( int x, int y );

    void Draw( Entity &player, Renderer *render );

    void set_tile( int x, int y, MaterialBlock tile );

    std::vector <Monster> monsters;
};


#endif
