#ifndef MINERL_SELECTSCENE_HPP
#define MINERL_SELECTSCENE_HPP

#include "IScene.hpp"

#include <map.h>
#include <player.h>

namespace Mine
{
    class SelectScene : public IScene
    {

    private:

        Cartographer cartographer = Cartographer( 120, 120 );

        Player player = Player( cartographer );

    public:

        SelectScene( );

        void Update( ) override;

        void Draw( ) override;

        void Clear( ) override;

        void Refresh( ) override;

        void HandleInputUser( int keyPressed ) override;

        int GetKeyPressed( ) override;
    };
}

#endif //MINERL_SELECTSCENE_HPP
