#ifndef MINERL_ISCENE_HPP
#define MINERL_ISCENE_HPP

#include <Entity.hpp>
#include <Render/Renderer.hpp>
#include <Render/Console.hpp>

namespace Mine
{
    class IScene
    {

    public:

        // Reference to Renderder | Only Variable
        Renderer *renderer = new Console( );

        IScene( );

        virtual ~IScene( );

        virtual void Update( ) = 0;

        virtual void Draw( ) = 0;

        virtual void Clear( ) = 0;

        virtual void Refresh( ) = 0;

        virtual void HandleInputUser( int keyPressed ) = 0;

        virtual int GetKeyPressed( ) = 0;

    };
}

#endif //MINERL_ISCENE_HPP
