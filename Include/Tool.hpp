#ifndef MINERL_TOOL_HPP
#define MINERL_TOOL_HPP

enum Supplies
{
    S_null = 0,
    S_auger,
    S_ladders,
    S_rope,
    S_supports,
    S_max
};

#endif //MINERL_TOOL_HPP
