#ifndef MINERL_DIRECTION_HPP
#define MINERL_DIRECTION_HPP

namespace Mine
{
    enum class Direction : short
    {
        NORTH,
        SOUTH,
        WEST,
        EAST
    };
}

#endif //MINERL_DIRECTION_HPP
