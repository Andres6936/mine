#include <Scene/IScene.hpp>
#include <Scene/SelectScene.hpp>

using namespace Mine;

int main( )
{
    //srand( time( NULL ));

    IScene *scene = new SelectScene( );

    while ( true )
    {
        scene->Clear( );
        scene->Update( );
        scene->Draw( );
        scene->Refresh( );

        int keyPressed = scene->GetKeyPressed( );

        if ( keyPressed == TK_CLOSE )
        {
            break;
        }

        scene->HandleInputUser( keyPressed );
    }

    delete scene;

    return 0;
}