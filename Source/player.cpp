#include "player.h"

#include <Geometry/Coordinate2D.hpp>

Player::Player( Cartographer &nMap )
{
    map = &nMap;

    stamina = maxStamina;
}

Player::~Player( ) = default;

int Player::GetDamage( )
{
    return 3;
}

void Player::WalkAt( Mine::Direction nDirection )
{
    using Mine::Direction;

    Coordinate2D coordinate;

    if ( nDirection == Direction::NORTH )
    {
        coordinate = Coordinate2D( posicion.x, GetCoordinateY( ) - 1 );
    }
    else if ( nDirection == Direction::SOUTH )
    {
        coordinate = Coordinate2D( posicion.x, GetCoordinateY( ) + 1 );
    }
    else if ( nDirection == Direction::WEST )
    {
        coordinate = Coordinate2D( GetCoordinateX( ) - 1, posicion.y );
    }
    else if ( nDirection == Direction::EAST )
    {
        coordinate = Coordinate2D( GetCoordinateX( ) + 1, posicion.y );
    }

    if ( map->ExistEnemiesAt( coordinate ))
    {
        AttackEnemy( map->GetEnemyAt( coordinate ));

        // El jugador debe de estar cansado con cada golpe.
        ReduceStamina( 3 );

        return;
    }

    if ( map->IsSolidBlockAt( coordinate ))
    {
        // No Walk, dig.
        if ( map->CanDigBlock( coordinate ))
        {
            // Añadimos al inventario el Mineral Ore extraido del bloque minado.
            AddMineralOreAtInventory( map->DigBlockAt( coordinate ));

            // El jugador debe de estar cansado con cada mineral que mina.
            ReduceStamina( 2 );
        }
    }
    else
    {
        // Walk.
        posicion.x = coordinate.x;
        posicion.y = coordinate.y;

        // El jugdador debe de estar cansado con cada paso que da.
        ReduceStamina( 1 );
    }
}

void Player::PutToolOfSupportInMap( Supplies tool )
{
    if ( tool == Supplies::S_ladders )
    {
        if ( HasEnoughLadders( ))
        {
            PutLadder( );
            inventory.ReduceLaddersInAUnit( );
        }
    }
    else if ( tool == Supplies::S_supports )
    {
        if ( HasEnoughSupport( ))
        {
            PutSupport( );
            inventory.ReduceSupportsInAUnit( );
        }
    }
}

void Player::AttackEnemy( Monster &objetive )
{
    objetive.TakeDamage( GetDamage( ));
}

const int Player::GetCoordinateX( )
{
    return posicion.x;
}

const int Player::GetCoordinateY( )
{
    return posicion.y;
}

void Player::AddMineralOreAtInventory( MaterialBlock mineral )
{
    inventory.AddMineralOre( mineral );
}

void Player::ReduceStamina( short amount )
{
    stamina = ( short ) stamina - amount;
}

const Coordinate2D &Player::GetCoordinate( )
{
    return posicion;
}

bool Player::HasEnoughLadders( )
{
    return inventory.HasEnoughLadders( );
}

void Player::PutLadder( )
{
    // Cuando el jugador decide poner una escalera, siempre
    // asumiremos que la posición donde el jugador se
    // encuentra es donde el desea poner la escalera.
    map->PutLadderAt( GetCoordinate( ));
}

bool Player::HasEnoughSupport( )
{
    return inventory.HasEnoughSupports( );
}

void Player::PutSupport( )
{
    // Cuando el jugador decide poner un soporte, siempre
    // asumiremos que la posición donde el jugador se
    // encuentra es donde el desea poner el soporte.
    map->PutSupportAt( GetCoordinate( ));
}
