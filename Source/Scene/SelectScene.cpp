#include <Scene/SelectScene.hpp>

Mine::SelectScene::SelectScene( ) = default;

void Mine::SelectScene::Update( )
{
    cartographer.UpdateBlocks( player );
    cartographer.UpdateEntities( player );
}

void Mine::SelectScene::Draw( )
{
    renderer->DrawStringAt( 61, 1, "Stamina: ", TypeColor::RED, TypeColor::BLACK );
}

void Mine::SelectScene::Clear( )
{
    renderer->Clear( );
}

void Mine::SelectScene::Refresh( )
{
    renderer->Refresh( );
}

int Mine::SelectScene::GetKeyPressed( )
{
    return renderer->GetKeyPressed( );
}

void Mine::SelectScene::HandleInputUser( int keyPressed )
{
    using Mine::Direction;

    if ( keyPressed == TK_UP )
    {
        player.WalkAt( Direction::NORTH );
    }
    else if ( keyPressed == TK_DOWN )
    {
        player.WalkAt( Direction::SOUTH );
    }
    else if ( keyPressed == TK_LEFT )
    {
        player.WalkAt( Direction::WEST );
    }
    else if ( keyPressed == TK_RIGHT )
    {
        player.WalkAt( Direction::EAST );
    }


    else if ( keyPressed == TK_W )
    {
        player.PutToolOfSupportInMap( Supplies::S_ladders );
    }
    else if ( keyPressed == TK_S )
    {
        player.PutToolOfSupportInMap( Supplies::S_supports );
    }
}