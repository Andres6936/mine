#include <Direction.hpp>
#include "Render/Console.hpp"

Console::Console( )
{
    TerminalOpen( );

    TerminalSet( "terminal: encoding=437" );
    TerminalSet( "window: size=80x24, cellsize=auto, title=MineRL" );
}

Console::~Console( )
{
    TerminalClose( );
}

void Console::Refresh( )
{
    TerminalRefresh( );
}

void Console::Clear( )
{
    TerminalClear( );
}

void Console::DrawCharAt( int X, int Y, int Char )
{
    TerminalPut( X, Y, Char );
}

void Console::DrawCharAt( int X, int Y, Glyph Char )
{
    SetForegroundColor( Char.foreground );
    SetBackgroundColor( Char.background );

    DrawCharAt( X, Y, Char.symbol );
}

void Console::DrawCharAt( int X, int Y, int Char, TypeColor foreground, TypeColor background )
{
    SetForegroundColor( foreground );
    SetBackgroundColor( background );

    DrawCharAt( X, Y, Char );
}

void Console::DrawStringAt( int X, int Y, std::string String )
{
    TerminalPrint( X, Y, String.c_str( ));
}

void Console::DrawStringAt( int X, int Y, std::string String, TypeColor foreground, TypeColor background )
{
    SetForegroundColor( foreground );
    SetBackgroundColor( background );

    DrawStringAt( X, Y, String );
}

void Console::SetBackgroundColor( TypeColor color )
{
    if ( color == TypeColor::BLACK )
    {
        TerminalBackColor( ColorFromName( "black" ));
    }
    else if ( color == TypeColor::WHITE )
    {
        TerminalBackColor( ColorFromName( "white" ));
    }
    else if ( color == TypeColor::GRAY )
    {
        TerminalBackColor( ColorFromName( "gray" ));
    }
    else if ( color == TypeColor::RED )
    {
        TerminalBackColor( ColorFromName( "red" ));
    }
    else if ( color == TypeColor::GREEN )
    {
        TerminalBackColor( ColorFromName( "green" ));
    }
    else if ( color == TypeColor::BLUE )
    {
        TerminalBackColor( ColorFromName( "blue" ));
    }
    else if ( color == TypeColor::CYAN )
    {
        TerminalBackColor( ColorFromName( "cyan" ));
    }
    else if ( color == TypeColor::MAGENTA )
    {
        TerminalBackColor( ColorFromName( "magenta" ));
    }
    else if ( color == TypeColor::BROWN )
    {
        TerminalBackColor( ColorFromName( "#654321" ));
    }
    else if ( color == TypeColor::YELLOW )
    {
        TerminalBackColor( ColorFromName( "yellow" ));
    }
}

void Console::SetForegroundColor( TypeColor color )
{
    if ( color == TypeColor::BLACK )
    {
        TerminalColor( ColorFromName( "black" ));
    }
    else if ( color == TypeColor::WHITE )
    {
        TerminalColor( ColorFromName( "white" ));
    }
    else if ( color == TypeColor::GRAY )
    {
        TerminalColor( ColorFromName( "gray" ));
    }
    else if ( color == TypeColor::RED )
    {
        TerminalColor( ColorFromName( "red" ));
    }
    else if ( color == TypeColor::GREEN )
    {
        TerminalColor( ColorFromName( "green" ));
    }
    else if ( color == TypeColor::BLUE )
    {
        TerminalColor( ColorFromName( "#6495ED" ));
    }
    else if ( color == TypeColor::CYAN )
    {
        TerminalColor( ColorFromName( "#00BFFF" ));
    }
    else if ( color == TypeColor::MAGENTA )
    {
        TerminalColor( ColorFromName( "magenta" ));
    }
    else if ( color == TypeColor::BROWN )
    {
        TerminalColor( ColorFromName( "#C19A6B" ));
    }
    else if ( color == TypeColor::YELLOW )
    {
        TerminalColor( ColorFromName( "yellow" ));
    }
}

void Console::DrawInterface( )
{
    DrawStringAt( 61, 1, "Stamina: ", TypeColor::RED, TypeColor::BLACK );
    DrawStringAt( 61, 2, "Storage: ", TypeColor::GREEN, TypeColor::BLACK );

    DrawStringAt( 61, 4, "Ladders: ", TypeColor::YELLOW, TypeColor::BLACK );
    DrawStringAt( 61, 5, "Rope: ", TypeColor::YELLOW, TypeColor::BLACK );
    DrawStringAt( 61, 6, "Supports: ", TypeColor::YELLOW, TypeColor::BLACK );
}

int Console::GetKeyPressed( )
{
    return TerminalRead( );
}

void Console::HandleInputUser( int nKeyPressed, Entity &player )
{
    using Mine::Direction;

    if ( nKeyPressed == TK_UP )
    {
        player.WalkAt( Direction::NORTH );
    }
    else if ( nKeyPressed == TK_DOWN )
    {
        player.WalkAt( Direction::SOUTH );
    }
    else if ( nKeyPressed == TK_LEFT )
    {
        player.WalkAt( Direction::WEST );
    }
    else if ( nKeyPressed == TK_RIGHT )
    {
        player.WalkAt( Direction::EAST );
    }


    else if ( nKeyPressed == TK_W )
    {
        player.PutToolOfSupportInMap( Supplies::S_ladders );
    }
    else if ( nKeyPressed == TK_S )
    {
        player.PutToolOfSupportInMap( Supplies::S_supports );
    }
}
