#include "Tile.hpp"

using Mine::TTile;

bool TTile::isBlocks( ) const
{
    return blocks;
}

bool TTile::isSupport( ) const
{
    return support;
}

int TTile::getDig( ) const
{
    return dig;
}

int TTile::getClimbCost( ) const
{
    return climbCost;
}

int TTile::getValue( ) const
{
    return value;
}

const std::string &TTile::getSymbols( ) const
{
    return symbols;
}

bool Mine::TTile::valuable( )
{
    return value > 0;
}

Mine::TTile::TTile( )
{
    blocks = false;
    support = false;
    dig = 0;
    climbCost = 0;
    value = 0;
    symbols = "";
}

Mine::TTile::TTile( bool B, bool S, int D, int CC, int V, std::string Sym, std::string Name ) :
        blocks( B ), support( S ),
        dig( D ), climbCost( CC ), value( V ),
        symbols( Sym ), name( Name )
{ }

Mine::TypeColor Mine::TTile::getColorForeground( ) const
{
    return colorFore;
}

Mine::TypeColor Mine::TTile::getColorBackground( ) const
{
    return colorBack;
}

Mine::TTile::TTile( TypeColor foreground, TypeColor background, bool B, bool S, int D, int CC, int V, std::string Sym,
                    std::string Name ) :
        blocks( B ), support( S ),
        dig( D ), climbCost( CC ), value( V ),
        symbols( Sym ), name( Name )
{
    colorFore = foreground;
    colorBack = background;
}
