#include "Glyph.h"
#include "options.h"
#include "window.h"
#include <sstream>

std::string Glyph::save_data( )
{
    std::stringstream ret;
    ret << symbol << " " << int( fg ) << " " << int( bg );
    return ret.str( );
};

void Glyph::load_data( std::istream &datastream )
{
    int fgtmp, bgtmp;
    datastream >> symbol >> fgtmp >> bgtmp;

    fg = EColor( fgtmp );
    bg = EColor( bgtmp );
}

bool Glyph::operator==( const Glyph &rhs )
{
    return ( rhs.fg == fg && rhs.bg == bg && rhs.symbol == symbol );
}

Glyph::Glyph( )
{
    symbol = 'x';
    fg = c_red;
    bg = c_white;
}

Glyph::Glyph( long S, EColor F, EColor B, TypeColor nForeground, TypeColor nBackground )
{
    symbol = S;
    fg = F;
    bg = B;

    foreground = nForeground;
    background = nBackground;
}

