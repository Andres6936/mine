#include "map.h"
#include "rng.h"
#include "player.h"
#include <Render/Console.hpp>
#include <TypeBlocks.hpp>

Mine::Block::Block( )
{
    material = T_null;
    representation = Glyph( 'x', c_red, c_white, TypeColor::RED, TypeColor::WHITE );
    durability = 0;
    isSeen = false;
    gravity = false;
}

void Mine::Block::setMaterial( MaterialBlock nMaterial )
{
    material = nMaterial;

    if ( nMaterial == MaterialBlock::T_empty )
    {
        representation.background = TypeColor::BLACK;
        representation.foreground = TypeColor::GRAY;
        representation.symbol = '.';

        gravity = false;
        durability = 0;
    }
    else if ( nMaterial == MaterialBlock::T_sky )
    {
        representation.background = TypeColor::CYAN;
        representation.foreground = TypeColor::CYAN;
        representation.symbol = ' ';

        gravity = false;
        durability = 0;
    }
    else if ( nMaterial == MaterialBlock::T_grass )
    {
        representation.background = TypeColor::CYAN;
        representation.foreground = TypeColor::GREEN;
        representation.symbol = '_';

        gravity = false;
        durability = 3;
    }
    else if ( nMaterial == MaterialBlock::T_shop )
    {
        representation.background = TypeColor::BLUE;
        representation.foreground = TypeColor::WHITE;
        representation.symbol = '=';

        gravity = false;
        durability = 0;
    }
    else if ( nMaterial == MaterialBlock::T_dirt )
    {
        representation.background = TypeColor::BROWN;
        representation.foreground = TypeColor::GRAY;

        // Posibles representaciones.
        std::string symbols = "   `~,.'^";

        // Escogemos únicamente un caracter.
        representation.symbol = symbols[ rand( ) % symbols.size( ) ];

        gravity = false;
        durability = 3;
    }
    else if ( nMaterial == MaterialBlock::T_boulder )
    {
        representation.background = TypeColor::BLACK;
        representation.foreground = TypeColor::GRAY;
        representation.symbol = '0';

        gravity = true;
        durability = 0;
    }
    else if ( nMaterial == MaterialBlock::T_ladder )
    {
        representation.background = TypeColor::BLACK;
        representation.foreground = TypeColor::RED;
        representation.symbol = 'H';

        gravity = false;
        durability = 0;
    }
    else if ( nMaterial == MaterialBlock::T_support )
    {
        representation.background = TypeColor::BLACK;
        representation.foreground = TypeColor::RED;
        representation.symbol = '|';

        gravity = false;
        durability = 0;
    }
    else if ( nMaterial == MaterialBlock::T_ladder_and_support )
    {
        representation.background = TypeColor::BLACK;
        representation.foreground = TypeColor::RED;
        representation.symbol = '#';

        gravity = false;
        durability = 0;
    }
    else if ( nMaterial == MaterialBlock::T_entrance )
    {
        representation.background = TypeColor::CYAN;
        representation.foreground = TypeColor::BLACK;
        representation.symbol = 'M';

        gravity = false;
        durability = 0;
    }
    else if ( nMaterial == MaterialBlock::T_coal )
    {
        representation.background = TypeColor::BROWN;
        representation.foreground = TypeColor::BLACK;
        representation.symbol = '#';

        gravity = false;
        durability = 5;
    }
    else if ( nMaterial == MaterialBlock::T_iron )
    {
        representation.background = TypeColor::BROWN;
        representation.foreground = TypeColor::WHITE;
        representation.symbol = '#';

        gravity = false;
        durability = 8;
    }
    else if ( nMaterial == MaterialBlock::T_copper )
    {
        representation.background = TypeColor::BROWN;
        representation.foreground = TypeColor::RED;
        representation.symbol = '#';

        gravity = false;
        durability = 10;
    }
    else if ( nMaterial == MaterialBlock::T_null )
    {
        representation.background = TypeColor::BLACK;
        representation.foreground = TypeColor::BLACK;
        representation.symbol = ' ';

        gravity = false;
        durability = 0;
    }
}

bool Mine::Block::isGravity( ) const
{
    return gravity;
}

void Mine::Block::setGravity( bool isGravity )
{
    gravity = isGravity;
}

bool Mine::Block::isSeen1( ) const
{
    return isSeen;
}

void Mine::Block::setIsSeen( bool nIsSeen )
{
    isSeen = nIsSeen;
}

int Mine::Block::getDurability( ) const
{
    return durability;
}

bool Mine::Block::isSolid( ) const
{
    if ( material == MaterialBlock::T_coal )
    {
        return true;
    }
    else if ( material == MaterialBlock::T_copper )
    {
        return true;
    }
    else if ( material == MaterialBlock::T_dirt )
    {
        return true;
    }
    else if ( material == MaterialBlock::T_iron )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Mine::Block::isMineralBlock( ) const
{
    if ( material == MaterialBlock::T_dirt )
    {
        return true;
    }
    else if ( material == MaterialBlock::T_coal )
    {
        return true;
    }
    else if ( material == MaterialBlock::T_copper )
    {
        return true;
    }
    else if ( material == MaterialBlock::T_iron )
    {
        return true;
    }
    else if ( material == MaterialBlock::T_grass )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Mine::Block::isWalkable( ) const
{
    return walkable;
}

void Mine::Block::setWalkable( bool isWalkable )
{
    walkable = isWalkable;
}

void Mine::Block::reduceDurability( int amount )
{
    durability -= amount;
}


Cartographer::Cartographer( const short width, const short heigth ) : WIDTH( width ), HEIGHT( heigth )
{
    tiles.reserve( WIDTH * HEIGHT );

    for ( int i = 0; i < WIDTH * HEIGHT; i++ )
    {
        tiles.emplace_back( );
    }

// Set the base terrain; sky, grass, and dirt.
    for ( int x = 0; x < WIDTH; x++ )
    {
        for ( int y = 0; y < HEIGHT; y++ )
        {
            if ( y < 12 )
            {
                tiles[ GetIndex( x, y ) ].setMaterial( T_sky );
                tiles[ GetIndex( x, y ) ].setIsSeen( true );
            }
            else if ( y == 12 )
            {
                tiles[ GetIndex( x, y ) ].setMaterial( T_grass );
                tiles[ GetIndex( x, y ) ].setIsSeen( true );
            }
            else
            {
                if ( y == 13 || y == 14 )
                {
                    tiles[ GetIndex( x, y ) ].setIsSeen( true );
                }
                if ( one_in( 10 ))
                {
                    tiles[ GetIndex( x, y ) ].setMaterial( T_boulder );
                }
                else
                {
                    tiles[ GetIndex( x, y ) ].setMaterial( T_dirt );
                }
            }
        }
    }

// We need a shop, of course...
    tiles[ GetIndex( 65, 12 ) ].setMaterial( T_shop );

// Now place some veins.
    int coal_veins = 5 * rng( 8, 12 );
    for ( int i = 0; i < coal_veins; i++ )
    {
        int x = rng( 10, 110 );
        int y = rng( 13, 25 );
        add_vein( T_coal, x, y, rng( 5, 10 ));
    }

    int iron_veins = 5 * rng( 8, 12 );
    for ( int i = 0; i < iron_veins; i++ )
    {
        int x = rng( 10, 110 );
        int y = rng( 20, 40 );
        add_vein( T_iron, x, y, rng( 4, 8 ));
    }

    int copper_veins = 5 * rng( 7, 11 );
    for ( int i = 0; i < copper_veins; i++ )
    {
        int x = rng( 10, 110 );
        int y = rng( 30, 60 );
        add_vein( T_copper, x, y, rng( 4, 7 ));
    }

    int empty_veins = 5 * rng( 20, 30 );
    int empty_y = 18;
    for ( int i = 0; i < empty_veins; i++ )
    {
        int x = rng( 10, 110 );
        if ( one_in( 5 ))
        {
            empty_y += rng( 1, 3 );
        }
        add_vein( T_empty, x, empty_y, rng( 3, 12 ));
    }
}

Cartographer::~Cartographer( ) = default;

void Cartographer::reset_null_tile( )
{
    null_tile.setMaterial( T_null );
}

void Cartographer::add_vein( MaterialBlock type, int x, int y, int size )
{
    set_tile( x, y, type );
    for ( int i = 0; i < size; i++ )
    {
        int move = ( y <= 15 ? rng( 1, 3 ) : rng( 1, 4 ));
        switch ( move )
        {
            case 1:
                x++;
                break;
            case 2:
                x--;
                break;
            case 3:
                y++;
                break;
            case 4:
                y--;
                break;
        }
        MaterialBlock id = get_tile_id( x, y );
        if ( id == T_dirt )
        {
            set_tile( x, y, type );
        }
        else if ( id != T_boulder && !one_in( 4 ))
        {
            i--;  // 75% of the time, don't use up part of our size
        }
    }
}

Mine::Block *Cartographer::get_tile( int x, int y )
{
    if ( x < 0 || x >= WIDTH )
    {
        reset_null_tile( );
        return &null_tile;
    }
    if ( y < 0 || y >= HEIGHT )
    {
        reset_null_tile( );
        return &null_tile;
    }

    return &( tiles[ GetIndex( x, y ) ] );
}

MaterialBlock Cartographer::get_tile_id( int x, int y )
{
    if ( x < 0 || x >= WIDTH )
    {
        return T_null;
    }
    if ( y < 0 || y >= HEIGHT )
    {
        return T_null;
    }
    return tiles[ GetIndex( x, y ) ].material;
}

void Cartographer::Draw( Entity &player, Renderer *render )
{
    constexpr short MAP_WIDTH = 60;
    constexpr short MAP_HEIGHT = 24;

    int x_min = player.GetCoordinateX( ) - MAP_WIDTH / 2;
    int y_min = player.GetCoordinateY( ) - MAP_HEIGHT / 2;

    int x_max = x_min + MAP_WIDTH - 1;
    int y_max = y_min + MAP_HEIGHT - 1;


    for ( int x = 0; x < MAP_WIDTH; x++ )
    {
        for ( int y = 0; y < MAP_HEIGHT; y++ )
        {
            int x_ter = x + x_min;
            int y_ter = y + y_min;

            if ( x_ter == player.GetCoordinateX( ) && y_ter == player.GetCoordinateY( ))
            {
                render->DrawCharAt( x, y, '@', TypeColor::WHITE, TypeColor::BLACK );
            }
            else
            {
                Mine::Block *tile = get_tile( x_ter, y_ter );

                if ( tile->isSeen1( ))
                {
                    render->DrawCharAt( x, y, tile->representation );
                }
                else
                {
                    render->DrawCharAt( x, y, ' ', TypeColor::BLACK, TypeColor::BLACK );
                }
            }
        }
    }
// Now, draw monsters.
    for ( auto &monster : monsters )
    {
        if ( monster.posx >= x_min && monster.posx <= x_max &&
             monster.posy >= y_min && monster.posy <= y_max )
        {
            int x = monster.posx - x_min, y = monster.posy - y_min;

            render->DrawCharAt( x, y, monster.type->sym );
        }
    }
// Finally, draw the message, if any...
    while ( !messages.empty( ))
    {
        render->DrawStringAt( 1, 0, messages[ 0 ], TypeColor::YELLOW, TypeColor::BLACK );

        messages.erase( messages.begin( ));
        if ( !messages.empty( ))
        {
            render->DrawStringAt( 1, 1, "More ... (Press Spacebar)", TypeColor::BLACK, TypeColor::RED );
        }

        while ( render->GetKeyPressed( ) != TK_SPACE )
        {

        }
    }
}

void Cartographer::set_tile( int x, int y, MaterialBlock tile )
{
    Mine::Block *tileptr = get_tile( x, y );
    tileptr->setMaterial( tile );
}

void Cartographer::ProcessBlockWithGravity( )
{

}

void Cartographer::SpawmEnemiesOfWayRandom( )
{

}

void Cartographer::ProcessMovementOfEntities( )
{

}


bool Cartographer::ExistEnemiesAt( Coordinate2D coordinate )
{
    for ( const Monster &monster: monsters )
    {
        if ( monster.posx == coordinate.x && monster.posy == coordinate.y )
        {
            return true;
        }
    }

    return false;
}

bool Cartographer::IsSolidBlockAt( Coordinate2D coordinate )
{
    using Mine::Block;

    Block block = tiles[ GetIndex( coordinate ) ];

    return block.isSolid( );
}

bool Cartographer::CanDigBlock( Coordinate2D coordinate )
{
    using Mine::Block;

    Block block = tiles[ GetIndex( coordinate ) ];

    return block.isMineralBlock( );
}

/**
 * Return the enemy in the position passed for parameter.
 * If exist more of one enemy, the method return only one enemy.
 * The enemy that the method return is the first that find in the coordinate.
 *
 * @precondtion The method ExistEnemiesAt( Coordinate ) should be called and
 * the result [value of return] should be True.
 *
 * @param coordinate Coordinate where is the enemy.
 * @return The enemy
 */
Monster &Cartographer::GetEnemyAt( Coordinate2D coordinate )
{
    for ( Monster &monster: monsters )
    {
        if ( monster.posx == coordinate.x && monster.posy == coordinate.y )
        {
            return monster;
        }
    }
}

/**
 * Es necesario resaltar, que actualmente los minerales y parte del terreno,
 * que puede ser minado tiene una cierta durabilidad, así que cuando el
 * jugador mina un bloque, probablemente necesitara de varios golpes (turnos)
 * para poder minar completamente el bloque.
 *
 * Los bloques minados, tienen la propiedad de devolver un
 * Mineral Ore del elemento del cual estaban compuestos.
 *
 * Si el bloque todavia no ha sido minado, ya sea porque necesita de otros
 * cuantos golpes (turnos) para ser minado, o porque el jugador no cuenta
 * con las herramientas necesarias, el bloque no tiene porque devolver su
 * mineral ore, asi que en ese caso, se utiliza un Mineral Ore nulo, es decir,
 * algo que represente que el bloque no ha sido minado.
 *
 * @param coordinate Coordenada del bloque a minar.
 * @return
 */
MaterialBlock Cartographer::DigBlockAt( Coordinate2D coordinate )
{
    using Mine::Block;

    Block &block = tiles[ GetIndex( coordinate ) ];

    if ( block.getDurability( ) > 0 )
    {
        // NOTE: Minar de acuerdo a la habilidad del jugador
        block.reduceDurability( 2 );

        // Aqui se haya nuestra representación de que el bloque no
        // ha sido minado.
        // Para cumplir con la firma del método es necesario que
        // con cada ejecución de este método devolvamos un Mineral Ore,
        // sin embargo, habra ocasiones donde un bloque no podrá ser
        // minado con un solo golpe (turno), y necesitara más de uno.
        // Asi que devolveremos MaterialBlock::T_null para representar
        // el estado donde el bloque necesitara más golpes (turnos) para
        // poder obtener algo de él.
        return MaterialBlock::T_null;
    }
    else
    {
        // Obtenemos el mineral ore del bloque.
        MaterialBlock mineralOre = block.material;

        // El bloque ha sido minado lo suficiente.
        block.setMaterial( MaterialBlock::T_empty );
        block.setWalkable( true );

        return mineralOre;
    }
}

int Cartographer::GetIndex( int X, int Y )
{
    return X + WIDTH * Y;
}

int Cartographer::GetIndex( Coordinate2D coordinate )
{
    return coordinate.x + WIDTH * coordinate.y;
}

void Cartographer::IliminateAreaAroundOfPoint( const Coordinate2D coordinate, const short radius )
{
    for ( int x = coordinate.x - radius; x <= coordinate.x + radius; x++ )
    {
        for ( int y = coordinate.y - radius; y <= coordinate.y + radius; y++ )
        {
            tiles[ GetIndex( x, y ) ].setIsSeen( true );
        }
    }
}

void Cartographer::PutLadderAt( Coordinate2D nCoordinate2D )
{
    using Mine::Block;

    Block &block = tiles[ GetIndex( nCoordinate2D ) ];

    block.setMaterial( MaterialBlock::T_ladder );
    block.setWalkable( true );

    block.representation.symbol = 'H';
    block.representation.foreground = TypeColor::RED;
}

void Cartographer::PutSupportAt( Coordinate2D nCoordinate2D )
{
    using Mine::Block;

    Block &block = tiles[ GetIndex( nCoordinate2D ) ];

    block.setMaterial( MaterialBlock::T_support );
    block.setWalkable( true );

    block.representation.symbol = '|';
    block.representation.foreground = TypeColor::RED;
}

void Cartographer::UpdateBlocks( Entity &entity )
{
    ProcessBlockWithGravity( );

    // Actualizamos el campo de visión, con centro en el jugador.
    IliminateAreaAroundOfPoint( entity.GetCoordinate( ), 2 );
}

void Cartographer::UpdateEntities( Entity &entity )
{
    ProcessMovementOfEntities( );
    SpawmEnemiesOfWayRandom( );
}
