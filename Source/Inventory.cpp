#include "Inventory.hpp"

Mine::Inventory::Inventory( )
{
    // Inicialmente, el jugador inicia
    // con un inventario vacio en minerales.
    // Pero con algunas herramientas.
    minerals[ MaterialBlock::T_coal ] = 0;
    minerals[ MaterialBlock::T_iron ] = 0;
    minerals[ MaterialBlock::T_copper ] = 0;

    tools[ Supplies::S_rope ] = 5;
    tools[ Supplies::S_auger ] = 1;
    tools[ Supplies::S_ladders ] = 20;
    tools[ Supplies::S_supports ] = 10;
}

/**
 * Incrementamos en una unidad el número de objetos del
 * mismo tipo {MaterialBlock} en el inventario.
 *
 * @param mineral Tipo de material.
 */
void Mine::Inventory::AddMineralOre( MaterialBlock mineral )
{
    minerals[ mineral ] = minerals[ mineral ] + 1;
}

bool Mine::Inventory::HasEnoughLadders( )
{
    return tools[ Supplies::S_ladders ] > 0;
}

void Mine::Inventory::ReduceLaddersInAUnit( )
{
    tools[ Supplies::S_ladders ] = tools[ Supplies::S_ladders ] - 1;
}

bool Mine::Inventory::HasEnoughSupports( )
{
    return tools[ Supplies::S_supports ] > 0;
}

void Mine::Inventory::ReduceSupportsInAUnit( )
{
    tools[ Supplies::S_supports ] = tools[ Supplies::S_supports ] - 1;
}
